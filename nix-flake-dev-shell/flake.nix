{
  description = "A basic dev shell";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";


  outputs = { self, nixpkgs, flake-utils, ... }:
    (flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
          };
        in
        {

          devShells.default = pkgs.mkShell {
            buildInputs = [
              pkgs.hello
              pkgs.cowsay
            ];
          };

          packages.default = pkgs.stdenv.mkDerivation {
            name = "my-package";
            src = ./.;

            buildPhase = ''
              ${pkgs.clang}/bin/clang -Wall -Wextra -Werror -O2 -std=c99 -pedantic main.c -o hello
            '';

            installPhase = ''
              mkdir -p $out/bin
              cp hello $out/bin
            '';

          };

        })
    );
}