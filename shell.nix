# shell.nix with terraform, npm, pandoc, git and make 

{ pkgs ? import <nixpkgs> { } }:

with pkgs;

mkShell {
  buildInputs = [
    # terraform
    nodejs
    pandoc
    gnumake
    git
    python3
  ];
}
