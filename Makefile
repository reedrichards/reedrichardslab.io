.PHONY: build open watch-open format clean serve

build: format
	ls | grep \.md$ | xargs basename -s .md | xargs -I _ pandoc -s --toc _.md -o public/_.html

open: build
	google-chrome public/index.html

watch-open: build
	while inotifywait -e close_write index.md; do make open; done

format:
	npx prettier --write *.md

clean:
	rm -rf public/*.html

serve:
	python -m http.server 