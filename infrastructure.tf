terraform {
    backend "http" {}
    required_providers {
        aws = {
            source  = "hashicorp/aws"
            version = "~> 3.0"
        }
    }
}

# variables 

variable "aws_region" {
  default = "us-east-1"
}

variable "domain" {
  default = "robw.fyi"
}

variable "gitlab_ip_addr" {
  default = "35.185.44.232"
  description = "For projects on GitLab.com, this IP is 35.185.44.232. For projects living in other GitLab instances (CE or EE), please contact your sysadmin asking for this information (which IP address is Pages server running on your instance). https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/#for-both-root-and-subdomains"
}

# providers

provider "aws" {
  region = var.aws_region
}

data "aws_route53_zone" "zone" {
  name         = var.domain
  private_zone = false
}

resource "aws_route53_record" "a_record" {
  zone_id = data.aws_route53_zone.zone.id
  name    = var.domain
  type    = "A"
  ttl = 300
  records = [var.gitlab_ip_addr]
}

resource "aws_route53_record" "verify_gitlab" {
  zone_id = data.aws_route53_zone.zone.id
  name = "_gitlab-pages-verification-code"
  type = "TXT"
  ttl = 300
  records = ["gitlab-pages-verification-code=8c81ca6d073e82a929cf051ed40bed80"]
}

resource "aws_route53_record" "nb_record" {
  zone_id = data.aws_route53_zone.zone.id
  name    = "nb.${var.domain}"
  type    = "A"
  ttl = 300
  records = ["54.186.174.84"]
}


resource "aws_route53_record" "noco" {
  zone_id = data.aws_route53_zone.zone.id
  name    = "noco.${var.domain}"
  type    = "A"
  ttl = 300
  records = ["54.186.174.84"]
}

resource "aws_route53_record" "wm" {
  zone_id = data.aws_route53_zone.zone.id
  name    = "wm.${var.domain}"
  type    = "A"
  ttl = 300
  records = ["54.186.174.84"]
}