---
title: Robert Wendt's Homepage
header-includes:
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico"/>
---

# If you are here from Noisebridge for Nix, click [here](https://gist.github.com/r33drichards/3ac361fa7b1d79fe216cf7e6f834e7aa)

# Contact Info & Other Useful Links

- [rwendt1337@gmail.com](mailto:rwendt1337@gmail.com)
- [Resume](resume.pdf)
- [Gitlab](https://gitlab.com/reedrichards)
- [Github](https://github.com/r33drichards)
- [Linkedin](https://www.linkedin.com/in/robert-wendt-150239186/)

# About Me

I am working on [Flakery](https://flakery.dev) in San Francisco, California. My interests and hobbies include software engineering, chess (and games in general), philosophy, math, reading and origami.

# [Flakery](https://flakery.dev)

Vercel for nixos flakes. You give me a nix flake url, I provision a nixos virtual machine using that flake. Also supports seeding the filesytem to load secrets.

# Creative Coding / Toys

- [game of life](https://wvdom-reedrichards-0bb8f83ee8d3e9f7f3ccf80ff71b748215f4820efcf5.gitlab.io/)
- [bubbles](/bubbles.html)
- [machine elves](/machine-elves.html)
- [cyber sigil](/cyber-sigil.html)
- [boxes](/boxes.html)
- [sierpinski](/sierpinski.html)
- [dots](/dots.html)
- [constellation](/constellation.html)
